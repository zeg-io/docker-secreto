# docker-secreto

"Secreto" is the word for secret, in Spanish.

This provides a super simple way to pull `docker secret` passwords into your code.

## Installation
`npm i -S docker-secreto`

## To pull all secrets in
```js
const dockerSecreto = require('docker-secreto')

let values = dockerSecreto()

console.info(values)
```

#### Output:
```js
{
  secret_name: 'secret value goes here',
  another_password: 'password123' // because that's what you should use
}
```

## To pull a specific secret
```js
const dockerSecreto = require('docker-secreto')

let values = dockerSecreto('secret_name', 'Default Value')

console.info(values)
```

#### Output if there is a match
```js
'secret value goes here'
```

## If there is no matching secret
```js
const dockerSecreto = require('docker-secreto')

let values = dockerSecreto('secret_name', 'Default Value')

console.info(values)
```

#### Output if there is not a match
```js
'Default Value'
```