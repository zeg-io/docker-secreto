const fs = require('fs')

const secretos = (key, defaultValue) => {
  let files = [],
      secrets

  try {
    files = fs.readdirSync('/run/secrets')
  } catch (ex) {
    // No /run/secrets directory
  }

  secrets = files.map(file => {
    try {
      const secret = fs.readFileSync(`/run/secrets/${file}`).toString()
      return { file, secret }
    } catch (ex) {

    }
  })

  let secretObjects = {}

  if (secrets.length > 0) {
    secrets.forEach(secret => {
      secretObjects[secret.file] = secret.secret
    })

    if (key)
      secretObjects = secretObjects[key] || defaultValue

    return secretObjects
  } else {
    if (defaultValue)
      secretObjects = defaultValue
    else if (key && !defaultValue)
      secretObjects[key] = {}

    return secretObjects
  }
}

module.exports = secretos
